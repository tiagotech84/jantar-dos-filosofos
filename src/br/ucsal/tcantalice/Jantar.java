package br.ucsal.tcantalice;

import br.ucsal.tcantalice.componentes.Mesa;
import br.ucsal.tcantalice.componentes.Filosofo;

public class Jantar {
    public static void main(String[] args) {
        Mesa mesa = new Mesa (5);

        for (int filosofo = 0; filosofo < Mesa.totalFilosofos; filosofo++) {
            mesa.adicionarFilosofo(new Filosofo(mesa, filosofo));
        }

        mesa.comecarJantar();
    }
}
