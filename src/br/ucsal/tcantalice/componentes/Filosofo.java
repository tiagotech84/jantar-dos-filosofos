package br.ucsal.tcantalice.componentes;

public class Filosofo implements Runnable {

    final static int TEMPO_MAXIMO = 100;

    private int id;
    private Mesa mesa;
    private Thread th;
    private EstadoFilosofo estadoFilosofo;

    public Filosofo(Mesa mesa, int id) {
        this.mesa = mesa;
        this.id = id;
        estadoFilosofo = EstadoFilosofo.PENSANDO;
        th = new Thread(this);
    }

    /**
     * Getter: id
     * @return int
     */
    public int id() {
        return this.id;
    }

    /**
     * Getter: estado
     * @return EstadoFilosofo
     */
    public EstadoFilosofo estado() {
        return this.estadoFilosofo;
    }

    /**
     * Setter: estado
     * @param estadoFilosofo Novo estado para o filósofo
     */
    public void estado(EstadoFilosofo estadoFilosofo) {
        this.estadoFilosofo = estadoFilosofo;
    }

    @Override
    public void run() {
        int tempo = 0;
        while (true)  {
            tempo = (int) (Math.random() * TEMPO_MAXIMO);
            pensar(tempo);

            pegarGarfos();

            tempo = (int) (Math.random() * TEMPO_MAXIMO);
            comer(tempo);

            devolverGarfos();
        }
    }

    /**
     * Inicia a Thread
     */
    public void start() {
        this.th.start();
    }

    /**
     * Pôe o filósofo para pensar
     * @param tempo
     */
    public void pensar (int tempo) {
        try {
            this.th.sleep(tempo);
        } catch (InterruptedException e) {
            System.out.println("O Filófoso pensou em demasia");
        }
    }

    /**
     * Pôe o filósofo para comer
     * @param tempo
     */
    public void comer (int tempo) {
        try {
            this.th.sleep(tempo);
        } catch (InterruptedException e) {
            System.out.println("O Filósofo comeu em demasia");
        }
    }

    /**
     * Tenta pegar os garfos da mesa
     */
    public void pegarGarfos() {
        mesa.pegarGarfos(id);
    }

    /**
     * Devolve os garfos da mesa
     */
    public void devolverGarfos() {
        mesa.devolverGarfos(id);
    }
}
