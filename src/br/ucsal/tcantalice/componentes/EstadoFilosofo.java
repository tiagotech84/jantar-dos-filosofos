package br.ucsal.tcantalice.componentes;

public enum EstadoFilosofo {
	PENSANDO("Pensando"), COMENDO("Comendo  "), FOME("Com Fome")   ;

	private String descricao;
	
	EstadoFilosofo(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

}

