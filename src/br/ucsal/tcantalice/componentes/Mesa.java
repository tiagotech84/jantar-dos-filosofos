package br.ucsal.tcantalice.componentes;

import static br.ucsal.tcantalice.componentes.EstadoFilosofo.*;
public class Mesa {

    public static int totalFilosofos = 0;

    private static int primeiroFilosofo;
    private static int ultimoFilosofo;

    boolean[] garfos;
    Filosofo[] filosofos;
    int[] tentativas;

    public Mesa(int numFilosofos){
        totalFilosofos = numFilosofos;

        primeiroFilosofo = 0;
        ultimoFilosofo = totalFilosofos - 1;

        filosofos = new Filosofo[totalFilosofos];
        garfos = new boolean[totalFilosofos];
        tentativas = new int[totalFilosofos];

        for(int i = 0; i < totalFilosofos; i++) {
            garfos[i] = true;
            tentativas[i] = 0;
        }
    }

    public void adicionarFilosofo(Filosofo filosofo) {
        filosofos[filosofo.id()] = filosofo;
    }

    public void comecarJantar() {
        for (Filosofo filosofo : filosofos) {
            filosofo.start();
        }
    }

    public synchronized void pegarGarfos(int filosofo) {
        filosofos[filosofo].estado(FOME);

        while (aEsquerda(filosofo).estado() == COMENDO || aDireita(filosofo).estado() == COMENDO)
        {
            try {
                tentativas[filosofo]++;
                wait();
            } catch (InterruptedException e) { }
        }

        garfos[garfoEsquerdo(filosofo)] = false;
        garfos[garfoDireito(filosofo)] = false;

        filosofos[filosofo].estado(COMENDO);
        imprimeEstadosFilosofos();
        imprimeGarfos();
    }

    public synchronized void devolverGarfos(int filosofo)
    {
        garfos[garfoEsquerdo(filosofo)] = true;
        garfos[garfoDireito(filosofo)] = true;

        if (aEsquerda(filosofo).estado() == FOME || aDireita(filosofo).estado() == FOME) {
            notifyAll();
        }
        filosofos[filosofo].estado(PENSANDO);

        imprimeEstadosFilosofos();
        imprimeGarfos();
    }

    public Filosofo aDireita(int filosofo) {
        return (filosofo == ultimoFilosofo) ? filosofos[primeiroFilosofo] : filosofos[filosofo + 1];
    }

    public Filosofo aEsquerda(int filosofo) {
        return (filosofo == primeiroFilosofo) ? filosofos[ultimoFilosofo] : filosofos[filosofo - 1];
    }

    public int garfoEsquerdo(int filosofo) {
        int garfo = filosofo;
        return garfo;
    }

    public int garfoDireito(int filosofo) {
        return (filosofo == ultimoFilosofo) ? 0 : filosofo + 1;
    }

    public void imprimeEstadosFilosofos ()
    {
        String texto = "*";
        System.out.print("Filósofos = [ ");

        for(Filosofo filosofo : filosofos) {
            String estado = filosofo.estado().getDescricao().toString().toUpperCase();
            System.out.printf(estado +  "\t");
        }

        System.out.println("]");
    }

    public void imprimeGarfos ()
    {
        System.out.print("Garfos    = [ ");

        for(boolean garfo : garfos) {
            String estado = garfo ? "LIVRE   " : "OCUPADO ";
            System.out.print(estado + "\t");
        }

        System.out.println("]");
    }

}
